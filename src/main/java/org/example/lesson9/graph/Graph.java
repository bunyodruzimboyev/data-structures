package org.example.lesson9.graph;

import java.util.*;

public class Graph {

    public class Node {
        private String value;

        public Node(String value) {
            this.value = value;
        }

        public String toString() {
            return value;
        }
    }

    private Map<String, Node> nodes = new HashMap<>();
    private Map<Node, ArrayList<Node>> adjacencyList = new HashMap<>();

    public void addEdge(String from, String to) {

        Node fromNode = nodes.get(from);
        if (fromNode == null) {
            throw new IllegalArgumentException();
        }

        Node toNode = nodes.get(to);
        if (toNode == null) {
            throw new IllegalArgumentException();
        }

        adjacencyList.get(fromNode).add(toNode); // Directed Graph
//        adjacencyList.get(toNode).add(fromNode); // UnDirected Graph

    }

    public void addNode(String label) {
        Node node = new Node(label);
        nodes.putIfAbsent(label, node);
        adjacencyList.put(node, new ArrayList<>());
    }


    public void print() {
        for (Node source : adjacencyList.keySet()) {
            ArrayList<Node> targets = adjacencyList.get(source);
            if (!targets.isEmpty()) {
                System.out.println(source.value + " is connected to " + targets.toString());
            }
        }
    }


    public void traverseBreadthFirst(String root) {
        Node node = nodes.get(root);
        if (node == null) return;

        Set<Node> visited = new HashSet<>();
        Queue<Node> queue = new ArrayDeque<>();
        queue.add(node);

        while (!queue.isEmpty()){
            Node current = queue.remove();
            if (visited.contains(current)) {
                continue;
            }
            System.out.println(current);
            visited.add(current);

            for (Node neighbour : adjacencyList.get(current)) {
                if (visited.contains(neighbour)) {
                    queue.add(neighbour);
                }
            }
        }
    }

    public void traverseDepthFirst(String root) {

        Node node = nodes.get(root);
        if (node == null) return;

        Set<Node> visited = new HashSet<>();
        Stack<Node> stack = new Stack<>();
        stack.push(node);

        while (!stack.isEmpty()) {
            Node current = stack.pop();

            if (visited.contains(current)) {
                continue;
            }

            System.out.println(current);
            visited.add(current);

            for (Node neighbour : adjacencyList.get(current)) {
                if (visited.contains(neighbour)) {
                    stack.push(neighbour);
                }
            }
        }
    }

    //Task 1
    public void removeNode(String label) {
        boolean isPresent = nodes.containsKey(label);
        if (!isPresent) {
            throw new IllegalArgumentException();
        }
        Node remove = nodes.remove(label);
        ArrayList<Node> nodeArrayList = adjacencyList.remove(remove);
    }


    //Task 2
    public void removeEdge(String from, String to){
        Node fromNode = nodes.get(from);
        if (fromNode == null) {
            throw new IllegalArgumentException();
        }

        Node toNode = nodes.get(to);
        if (toNode == null) {
            throw new IllegalArgumentException();
        }
        adjacencyList.get(fromNode).remove(toNode);
    }

}
