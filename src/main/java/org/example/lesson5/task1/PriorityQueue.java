package org.example.lesson5.task1;

import java.util.Arrays;

public class PriorityQueue {

    private int[] items;
    private int count;
    private int front;
    private int rear;

    public PriorityQueue(int n) {
        items = new int[n];
    }

    public void enqueue(int item) {
        if (rear == items.length) {
            throw new StackOverflowError();
        }
        items[rear++] = item;
    }

    public int dequeue() {
        if (front == items.length) {
            throw new IllegalStateException();
        }
        int item = items[front];
        items[front] = 0;
        front++;
        return item;
    }

    public boolean isEmpty() {
        return rear == front;
    }

    public void print() {
        System.out.println(Arrays.toString(items));
    }
}
