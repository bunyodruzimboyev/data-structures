package org.example.lesson5.task2;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class Solution {
    public static void main(String[] args) {
        Queue<Integer> numbers = new LinkedList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(5);
        numbers.add(6);
        numbers.add(7);
        numbers.add(8);
        numbers.add(9);
        numbers.add(10);
        System.out.println(numbers);


        int size = numbers.size();
        System.out.print("Enter k: ");
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        if (k > size) {
            System.out.println(k + " is bigger than " + size);
            throw new IllegalArgumentException();
        }

        Queue<Integer> nums2 = new LinkedList<>();
        Stack<Integer> numsStack = new Stack<>();
        for (int i = 0; i < k; i++) {
            numsStack.add(numbers.peek());
            numbers.poll();
        }
        for (int i = 0; i < k; i++) {
            nums2.add(numsStack.peek());
            numsStack.pop();
        }
        for (int i = 0; i < size - k; i++) {
            nums2.add(numbers.peek());
            numbers.poll();
        }
        numbers = nums2;
        System.out.println(numbers);

    }
}
