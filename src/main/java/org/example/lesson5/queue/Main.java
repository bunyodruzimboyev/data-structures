package org.example.lesson5.queue;

import java.util.PriorityQueue;

public class Main {
    public static void main(String[] args) {
        PriorityQueue<Integer> numbers = new PriorityQueue<>();
        numbers.add(10);
        numbers.add(30);
        numbers.add(1);
        numbers.add(22);
        numbers.add(121);
        numbers.add(500);
        numbers.add(50);
        numbers.add(80);
        numbers.add(550);
        numbers.add(780);
        numbers.add(800);
        System.out.println(numbers);
        System.out.println(numbers.poll());
        System.out.println(numbers);
    }
}
