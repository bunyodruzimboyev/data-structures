package org.example.lesson10.undirectedGraph;

import org.example.lesson9.graph.Graph;

import java.util.*;

public class WeightedGraph {


    private class Edge {
        private Node from;
        private Node to;
        private int weight;

        public Edge(Node from, Node to, int weight) {
            this.from = from;
            this.to = to;
            this.weight = weight;
        }

        @Override
        public String toString() {
            return from + " -> " + to;
        }
    }

    private class Node {
        private String value;
        private List<Edge> edges = new ArrayList<>();

        public Node(String value) {
            this.value = value;
        }

        public String toString() {
            return value;
        }

        public void addEdge(Node to, int weight) {
            edges.add(new Edge(this, to, weight));
        }

        public List<Edge> getEdges() {
            return edges;
        }
    }
    private Map<String, Node> nodes = new HashMap<>();

    public void addNode(String label) {
        nodes.putIfAbsent(label, new Node(label));
    }

    public void addEdge(String from, String to, int weight) {

        Node fromNode = nodes.get(from);
        if (fromNode == null) {
            throw new IllegalArgumentException();
        }

        Node toNode = nodes.get(to);
        if (toNode == null) {
            throw new IllegalArgumentException();
        }
        fromNode.addEdge(toNode, weight);
        fromNode.addEdge(fromNode, weight);

    }

    public void print() {
        for (Node node : nodes.values()) {
            List<Edge> edges = node.getEdges();
            if (!edges.isEmpty()) {
                System.out.println(node + " is connected to " + edges.toString());
            }
        }
    }

    private class NodeEntry{
        private Node node;
        private int priority;

        public  NodeEntry(Node node, int priority){
            this.node = node;
            this.priority = priority;
        }
    }

    public int getShortestDistance(String from, String to) {
//        Node fromNode = nodes.get(from);
//
//        Map<Node, Integer> distance = new HashMap<>();
//        for (Node node : nodes.values()) {
//            distance.put(node, Integer.MAX_VALUE);
//        }
//        distance.replace(nodes.get(from), 0);
//
//        Set<Node> visited = new HashSet<>();
//        PriorityQueue<Node> queue = new PriorityQueue<>(
//                Comparator.comparingInt(nodeEntry -> nodeEntry.priority)
//        );
//        queue.add(new NodeEntry(fromNode, 0));
//
//        while (!queue.isEmpty()) {
//          current =  queue.remove().node;
//          visited.add(current);
//
//
//        }
        return 0;
    }

}
