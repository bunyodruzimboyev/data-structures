package org.example.lesson10.undirectedGraph;

public class Main {
    public static void main(String[] args) {

        WeightedGraph graph = new WeightedGraph();
        graph.addNode("A");
        graph.addNode("B");
        graph.addNode("C");
        graph.addNode("D");
        graph.addNode("E");

        graph.addEdge("A", "B",2);
        graph.addEdge("A", "E",3);
        graph.addEdge("B", "E",1);
        graph.addEdge("C", "A",5);
        graph.addEdge("A", "B",12);
        graph.addEdge("A", "B",9);
        graph.addEdge("A", "B",4);
    }
}
