package org.example.lesson7.tree;

public class Main {
    public static void main(String[] args) {

        BinaryTree tree = new BinaryTree();
        tree.insert(25);
        tree.insert(15);
        tree.insert(50);
        tree.insert(75);
        tree.insert(55);
        tree.insert(4578);
        tree.insert(7);
        tree.insert(125);
        tree.insert(375);
        tree.insert(415);
        tree.insert(753);
        tree.insert(95);
        tree.insert(5);
        tree.insert(885);

        tree.traversalPostOrder();

        System.out.println("Height: " + tree.height());

        tree.printAllLeaves();

        tree.min();
        System.out.println();
        tree.max();


//        tree.isBinarySearchTree();

        BinaryTree binaryTree = new BinaryTree();
        binaryTree.insert(10);
        binaryTree.insert(15);
//        binaryTree.insert(5);
//        binaryTree.insert(4);
//        binaryTree.insert(6);
//        binaryTree.insert(16);
//        binaryTree.insert(20);
//        binaryTree.insert(13);
        System.out.println("------");
        binaryTree.isBinarySearchTree();
        System.out.println("------");

        binaryTree.printNodesAtDistance(4);
    }
}
