package org.example.lesson7.tree;

public class BinaryTree {

    private class Node {
        private int value;
        private Node left;
        private Node right;

        public Node(int value) {
            this.value = value;
        }
    }

    private Node root;

    public void insert(int item) {
        if (root == null) {
            root = new Node(item);
            return;
        }

        Node current = root;
        while (true) {
            if (item > current.value) {//right
                if (current.right == null) {
                    current.right = new Node(item);
                    break;
                }
                current = current.right;
            } else {//left
                if (current.left == null) {
                    current.left = new Node(item);
                    break;
                }
                current = current.left;
            }
        }
    }

    public void traversalPreOrder() {
        traversalPreOrder(root);
    }

    public void traversalPreOrder(Node root) {
        if (root == null) return;

        System.out.println(root.value);
        traversalPreOrder(root.left);
        traversalPreOrder(root.right);
    }


    public void traversalPostOrder() {
        traversalPostOrder(root);
    }

    public void traversalPostOrder(Node root) {
        if (root == null) return;

        System.out.println(root.value);
        traversalPostOrder(root.left);
        traversalPostOrder(root.right);
    }

    public int height() {
        return height(root);
    }

    public int height(Node root) {
        if (root == null) return 0;
        int leftHeight = height(root.left);
        int rightHeight = height(root.right);

        if (leftHeight > rightHeight) {
            return (leftHeight + 1);
        } else {
            return (rightHeight + 1);
        }
    }

    public void traversalLevelOrder() {
        int height = height();

        for (int i = 1; i <= height; i++) {
            traversalLevelOrder(root, i);
        }
    }

    public void traversalLevelOrder(Node root, int level) {
        if (root == null) return;
        if (level == 1) {
            System.out.println(root.value);
        } else {
            traversalLevelOrder(root.left, level - 1);
            traversalLevelOrder(root.right, level - 1);
        }
    }

    public boolean find(int item) {
        Node current = root;

        while (current != null) {
            if (item < current.value) {
                current = current.left;
            } else if (item > current.value) {
                current = current.right;
            } else {
                return true;
            }
        }
        return false;
    }


    //TASK 1
    public void printAllLeaves() {
        System.out.print("{ ");
        printAllLeaves(root);
        System.out.println("}");
    }

    public void printAllLeaves(Node root) {
        if (root == null) return;
        if (root.left == null && root.right == null && root != null) {
            System.out.print(root.value + "; ");
        }
        printAllLeaves(root.left);
        printAllLeaves(root.right);
    }

    //TASK2
    public void min() {
        System.out.println("min value: " + min(root));
    }

    public int min(Node root) {
        if (root.left == null) {
            return root.value;
        } else {
            return min(root.left);
        }
    }


    //TASK3
    public void max() {
        System.out.println("max value: " + max(root));
    }

    public int max(Node root) {
        if (root.right == null) {
            return root.value;
        } else {
            return max(root.right);
        }
    }


    //TASK4
    public void isBinarySearchTree() {
        System.out.println(isBinarySearchTree(root));
    }

    public boolean isBinarySearchTree(Node root) {
        if (root == null) return true;
        if (root.left == null && root.right == null) return true;
        if ((root.left == null && root.right != null) || (root.right == null && root.left != null)) return false;
        if (isBinarySearchTree(root.right) && isBinarySearchTree(root.left)) return true;
        return false;
    }


    //TASK5
    public void printNodesAtDistance(int k) {
        printNodesAtDistance(root, k);
    }

    public void printNodesAtDistance(Node root, int k) {
        if (root != null) {
            if (k == 1) {
                System.out.print(root.value + "; ");
            } else {
                printNodesAtDistance(root.left, k - 1);
                printNodesAtDistance(root.right, k - 1);
            }
        }
    }
}
