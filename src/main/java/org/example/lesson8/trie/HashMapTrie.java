package org.example.lesson8.trie;


import java.util.HashMap;
import java.util.Map;

public class HashMapTrie {

    private class Node {
        private char value;
        private boolean isWord;
        private HashMap<Character, Node> child = new HashMap<>();

        public Node(char value) {
            this.value = value;
        }
    }

    Node root = new Node(' ');

    public void insert(String word) {
        Node current = root;
        for (char c : word.toCharArray()) {
            if (!current.child.containsKey(c)) {
                current.child.put(c, new Node(c));
            }
            current = current.child.get(c);
        }
        current.isWord = true;
    }

    public boolean search(String word) {

        Node current = root;
        for (char c : word.toCharArray()) {
            if (!current.child.containsKey(c)) return false;
            for (Map.Entry<Character, Node> entry : current.child.entrySet()) {
                if (entry.getKey() == c) {
                    current = entry.getValue();
                    break;
                }
            }
        }
        return current.isWord;
    }

    public boolean startWith(String prefix) {
        Node current = root;
        for (char c : prefix.toCharArray()) {
            if (!current.child.containsKey(c)) return false;
            for (Map.Entry<Character, Node> entry : current.child.entrySet()) {
                if (entry.getKey() == c) {
                    current = entry.getValue();
                    break;
                }
            }
        }
        return true;
    }

    public void print() {
        print(root);
    }

    private void print(Node root) {
        if (root != null) {
            System.out.println(root.value);
            for (Map.Entry<Character, Node> entry : root.child.entrySet()) {
                print(entry.getValue());
            }
        }
    }
}

