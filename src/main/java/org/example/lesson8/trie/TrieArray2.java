package org.example.lesson8.trie;


public class TrieArray2 {

    public class  Node{
        private char value;
        private boolean isWord;
        private Node[] children;

        public Node(char value){
            this.value = value;
            this.children = new Node[26];
            this.isWord = false;
        }
    }
    Node root;

    public TrieArray2(){
        root = new Node('\0');
    }

    /**
     * Lesson 8, Task 1.
     * Remove word
     * @param word String
     */
    public void remove(String word){
        if (!search(word) || word.equals(""))
            return;

        Node curr = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            int index = c - 'a';
            curr = curr.children[index];
        }
        curr.isWord = false;

        for (Node child : curr.children) {
            if (child != null)
                return;
        }

        char c = word.charAt(0);
        int index = c - 'a';
        root.children[index] = null;
    }






    public void insert(String word){
        Node curr = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            int index = c - 'a';

            if (curr.children[index] == null)
                curr.children[index] = new Node(c);

            curr = curr.children[index];
        }
        curr.isWord = true;
    }


    public boolean search(String word){
        Node curr = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            int index = c - 'a';

            if (curr.children[index] == null)
                return false;
            curr = curr.children[index];
        }
        return curr.isWord;
    }

    public boolean startWith(String prefix){
        Node curr = root;
        for (int i = 0; i < prefix.length(); i++) {
            char c = prefix.charAt(i);
            int index = c - 'a';

            if (curr.children[index] == null)
                return false;
            curr = curr.children[index];
        }
        return true;
    }

    public void traverse(){
        traverse(root);
    }

    private void traverse(Node root){
        if (root != null){

            System.out.println(root.value);
            for (Node child: root.children)
                traverse(child);

        }

    }
}
