package org.example.lesson8.trie;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

public class TrieArray {

    private class Node {
        private char value;
        private boolean isWord;
        private Node[] child;

        public Node(char value) {
            this.value = value;
            this.child = new Node[26];
            this.isWord = false;
        }
    }

    Node root;

    public TrieArray() {
        root = new Node('\0');
    }


    public void insert(String word) {
        Node current = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            int index = c - 'a';

            if (current.child[index] == null) {
                current.child[index] = new Node(c);
            }
            current = current.child[index];
        }
        current.isWord = true;
    }

    public void remove(String word) {

        if (word.length() == 0) return;

        Node current = root;
        for (int i = 0; i < word.length(); i++) {

            char ch = word.charAt(i);
            int index = ch - 'a';

            if (current.child[index] == null) break;

            if (i == word.length() - 1) {
                if (current.child[index].isWord)
                    current.child[index].isWord = false;

                current = current.child[index];

                boolean check = false;
                for (int j = 0; j < 26; j++) {
                    if (current.child[j] != null) {
                        check = true;
                        break;
                    }
                }
                if (!check) {
                    current = null; //return;
                }
            }
            if (current != null)
                current = current.child[index];

        }
    }


//    public void remove(String word){
//        if (!search(word) || word.equals(""))
//            return;
//
//        Node curr = root;
//        for (int i = 0; i < word.length(); i++) {
//            char c = word.charAt(i);
//            int index = c - 'a';
//            curr = curr.child[index];
//        }
//        curr.isWord = false;
//
//        for (Node child : curr.child) {
//            if (child != null)
//                return;
//        }
//
//        char c = word.charAt(0);
//        int index = c - 'a';
//        root.child[index] = null;
//    }
//


    public boolean search(String word) {
        Node current = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            int index = c - 'a';
            if (current.child[index] == null) return false;
            current = current.child[index];
        }
        return current.isWord;
    }

    public boolean startWith(String prefix) {
        Node current = root;
        for (int i = 0; i < prefix.length(); i++) {
            char c = prefix.charAt(i);
            int index = c - 'a';
            if (current.child[index] == null) return false;
            current = current.child[index];
        }
        return true;
    }


    public void print() {
        print(root);
    }

    private void print(Node root) {
        if (root != null) {
            System.out.println(root.value);
            for (Node child : root.child) {
                print(child);
            }
        }
    }
}
