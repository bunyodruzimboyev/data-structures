package org.example.lesson8.trie;

public class Main {
    public static void main(String[] args) {
//        TrieArray trie = new TrieArray();
//        trie.insert("cat");
//        trie.insert("car");
//        trie.print();
//
//        System.out.println("-------");
//        trie.remove("cat");
//        System.out.println(trie.search("cat"));
//        trie.print();
//
//

//        TrieArray2 trieArray2 = new TrieArray2();
//        trieArray2.insert("cat");
//        trieArray2.insert("car");
//        trieArray2.traverse();
//        System.out.println("------");
//        trieArray2.remove("cat");
//        trieArray2.traverse();
        HashMapTrie trie = new HashMapTrie();
        trie.insert("wood");
        trie.insert("word");

        System.out.println(trie.search("woodman"));
        System.out.println(trie.startWith("wod"));
        System.out.println("-------");
        trie.print();

    }
}
