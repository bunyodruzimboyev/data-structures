package org.example.lesson2.arrayList;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        /**
         * arrayning asosiy kamchiligi uning uzunligini oldindan belgilab qo'yishda.
         * ArrayList.da bu kamchilik tuzatilgan,ya'ni uning uzunligi dynamic static emas.
         * ArrayList == dynamic array
         */


        ArrayList list = new ArrayList(4);
        list.insert(2);
        list.insert(3);
        list.insert(4);
        list.insert(5);
        list.insertAt(88, 2);
//        list.removeAt(1);
        list.print();
        System.out.println("----------------------------------");
        list.reverse();
        list.print();

//        System.out.println(list.indexOf(4));

        ArrayList list1 = new ArrayList(0);
//        list1.max();

        System.out.println("----------------------------------");
        list.print();
        int[] intersect = list.intersect(new int[]{2, 2, 3, 123, 231});
        System.out.println(Arrays.toString(intersect));
    }
}
