package org.example.lesson2.arrayList;

public class ArrayList {
    private int[] items;
    private int count;

    public ArrayList(int length) {
        items = new int[length];
    }



    //Time Complexity - O(count - index) => O(n)
    public void removeAt(int index) {
        // 2 3 4 5 6 count
        // 2 3 5 6 6 count --  soni bittaga kamaygani evazga oxirgi element o'tindan chiqib ketadi

        // invalid index
        if (index <0 || index >= count) {
            throw new IllegalArgumentException();
        }

        //shift items
        for (int i = index; i < count; i++) {
            items[i] = items[i + 1];
        }
        count--;
    }



    //uzunligi o'zgarganda. Time Complexity - O(count) +  O(count - index) = O(2count - index) => O(n)
    //uzunligi o'zgarmaganda. Time Complexity - O(count - index)  => O(n)
    //BARIBIR UMUMIY QILIB AYTGANDA Time Complexity - O(n)
    public void insertAt(int item, int index) {

        // resize items
        if (items.length == count) {
            int[] newItems = new int[count * 2];
            for (int i = 0; i < count; i++) {
                newItems[i] = items[i];
            }
            items = newItems;
        }

        //shift items
        for (int i = count - 1; i >= index; i--) {
            items[i + 1] = items[i];
        }

        // add item
        count++;
        items[index] = item;
    }



    // Time Complexity - O(count) => O(n)
    public void insert(int item) {
        // resize items
        if (items.length == count) {
            int[] newItems = new int[count * 2];
            for (int i = 0; i < count; i++) {
                newItems[i] = items[i];
            }
            items = newItems;
        }
        // add item
        items[count++] = item;
    }



    //Time complexity - O(count) => O(n)
    public int indexOf(int item) {
        for (int i = 0; i < count; i++) {
            if (items[i] == item) {
                return i;
            }
        }
        return -1;
    }



    //Time Complexity - O(count) => O(n)
    public void print() {
        for (int i = 0; i < count; i++) {
            System.out.println(items[i]);
        }
    }


    //Time Complexity - O(count - 1) => O(n)
    public void max(){
        //o'lchamini tekshiramiz
        if (count == 0) {
            throw new IllegalArgumentException();
        }
        int maxElement = items[0];
        for (int i = 1; i < count; i++) {
            if (items[i] > maxElement) {
                maxElement = items[i];
            }
        }
        System.out.println(maxElement);
    }




    //Time Complexity - O(count - 1) => O(n)
    public void min(){
        //o'lchamini tekshiramiz
        if (count == 0) {
            throw new IllegalArgumentException();
        }
        int minElement = items[0];
        for (int i = 1; i < count; i++) {
            if (items[i] < minElement) {
                minElement = items[i];
            }
        }
        System.out.println(minElement);
    }



    // qoidaga ko'ra 2 ga bo'lish 1/2 ga ko'paytirish demakdir, shuning u-n BigO notationga ko'ra n oldidagi koeffitsiyntni tashlab yubordim
    // Time Complexity - O(count/2) = O((1/2) * count) => O(count) => O(n)
    public void reverse(){
        int m;
        for (int i = 0; i < count/2; i++) {
            m = items[i]; items[i] = items[count - i - 1]; items[count - i - 1] = m;
        }
    }


    // Time Complexity - O(n^3)
    public int[] intersect(int[] arr){

        int[] common = new int[arr.length];
        int commonCount = 0;
        boolean alreadyExist = false;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < count; j++) {
                if (arr[i] == items[j]) {
                    alreadyExist = false;
                    for (int k = 0; k < commonCount; k++) {
                        if (common[k] == arr[i]) {
                            alreadyExist = true; break;
                        }
                    }
                    if (!alreadyExist) {
                        common[commonCount++] = arr[i];
                    }
                }
            }
        }
        int[] newCommon = new int[commonCount];
        for (int i = 0; i < commonCount; i++) {
            newCommon[i] = common[i];
        }
        return newCommon;
    }

}
