package org.example.lesson1.asymtomaticAnalysis;

public class Task3 {
    public static void main(String[] args) {

        //Space complexity - O(1) + O(1) = O(2) => O(1)
        int a = 0;
        int n = 100;

        //Time complexity - O(1/2 * n^2) => O(n^2)
        for (int i = 0; i < n; i++) {
            for (int j = n; j > i; j--) {
                a = a + i + j;
            }
        }
    }
}
