package org.example.lesson1.asymtomaticAnalysis;

public class Task2 {
    public static void main(String[] args) {

        //Space complexity - O(2) + O(1) = O(3) => O(1)
        int a = 0, b = 0; //O(2)
        int n = 100; //O(1)

        //Time complexity - O(n^2) + O(n) = O(n^2 + n) => O(n^2)
        //O(n^2)
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a = a + j;
            }
        }

        //O(n)
        for (int k = 0; k < n; k++) {
            b = b + k;
        }
    }
}
