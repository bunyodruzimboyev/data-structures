package org.example.lesson1.asymtomaticAnalysis;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {

        //Time complexity - O(sqrt(n))
        //Space complexity - O(1)
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a natural number: ");

        int number = scanner.nextInt(); //O(1)
        if (isPrime(number))
            System.out.println(number + " is prime number.");
        else
            System.out.println(number + " is not prime number.");

    }

    public static boolean isPrime(int number) {
        if (number == 1) {
            return false;
        } else {
            for (int i = 2; i * i < number; i++) {
                if (number % i == 0) {
                    return false;
                }
            }
            return true;
        }
    }
}



