package org.example.lesson1.asymtomaticAnalysis;

public class Task5 {
    public static void main(String[] args) {

        //Space complexity - O(1) + O(1) = O(2) => O(1)
        int a = 0;
        int n = 100;

        //Time complexity - O(n) * O(logn) = O(n logn)
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j = j * 2) {
                a = a + j;
            }
        }
    }
}
