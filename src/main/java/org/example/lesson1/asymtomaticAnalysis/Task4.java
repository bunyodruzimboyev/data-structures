package org.example.lesson1.asymtomaticAnalysis;

public class Task4 {
    public static void main(String[] args) {

        //Space complexity - O(1) + O(1) = O(2) => O(1)
        int a = 0;
        int n = 100;

        //Time complexity - O(log n)
        for (int i = 0; i < n; i = i * 2) {
            a = a + i;
        }
    }
}
