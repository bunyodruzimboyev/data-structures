package org.example.lesson1.asymtomaticAnalysis;

public class Task1 {
    public static void main(String[] args) {

        //Space complexity - O(2) + O(1) = O(3) => O(1)
        int a = 0, b = 0; //O(2)
        int n = 100; //O(1)

        //Time complexity - O(n) + O(n) = O(2n) => O(n)
        //O(n)
        for (int i = 0; i < n; i++) {
            a = a + i;
        }

        //O(n)
        for (int j = 0; j < n; j++) {
            b = b + j;
        }
    }
}
