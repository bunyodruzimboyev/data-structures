package org.example.lesson3.linkedList;

import java.util.NoSuchElementException;

public class LinkedList {

    private class Node {
        private int value;
        private Node next;

        public Node(int value) {
            this.value = value;
        }
    }


    private Node first;
    private Node last;
    private int size = 0;


    //Time Complexity - O(n)
    public int[] toArray() {

        int[] array = new int[size];
        Node current = first;
        int index = 0;

        while (current.next != null) {
            array[index] = current.value;
            current = current.next;
            index++;
        }
        return array;
    }


    //Time Complexity - O(1)
    public int size() {
        return size;
    }


    //Time Complexity - O(n)
    public void removeLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        if (first == last) {
            first = last = null;
        } else {
            Node previous = getPrevious(last);
            last = previous;
            last.next = null;
        }
        size--;
    }


    //Time Complexity - O(n)
    private Node getPrevious(Node node) {
        Node current = first;
        while (current != null) {
            if (current.next == node) {
                return current;
            }
            current = current.next;
        }
        return null;
    }


    //Time Complexity - O(1)
    public void removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        if (first == last) {
            first = last = null;
        } else {
            Node second = first.next;
            first.next = null;
            first = second;
        }
        size--;
    }


    //Time Complexity - O(n)
    public boolean contains(int item) {
        return indexOf(item) != -1;
    }


    //Time Complexity - O(n)
    public int indexOf(int item) {
        int index = 0;
        Node current = first;
        while (current != null) {
            if (current.value == item) {
                return index;
            }
            current = current.next;
            index++;
        }
        return -1;
    }


    //Time Complexity - O(1)
    public void addFirst(int item) {
        Node node = new Node(item);

        if (isEmpty()) {
            first = last = node;
        } else {
            node.next = first;
            first = node;
        }
        size++;
    }


    public void addLast(int item) {
        Node node = new Node(item);
        if (isEmpty()) {
            first = last = node;
        } else {
            last.next = node;
            last = node;
        }
        size++;
    }


    public boolean isEmpty() {
        return first == null;
    }

    public void print() {
        Node temp = first;

        while (temp != null) {
            System.out.print(temp.value + " ");
            temp = temp.next;
        }
        System.out.println();
    }


    //Task 1
    //Time Complexity - O(n)
    public boolean search(int item) {
        Node current = first;
        while (current != null) {
            if (item == current.value) {
                return true;
            }
            current = current.next;
        }
        return false;
    }


    //Task 2
    //Time Complexity - O(n)
    public void lastIndexOf(int item) {

        if (first == last && first.value == item) {
            System.out.println(0);
        } else {
            System.out.println(-1);
        }

        Node current = first;
        int position = 0;
        int index = -1;

        while (current != null) {
            if (item == current.value) {
                index = position;
            }
            current = current.next;
            position++;
        }

        System.out.println(index);
    }


    //Task 3
    //Time Complexity - O(n)
    public void printMiddle() {

        if (size == 0) {
            throw new NoSuchElementException();
        }

        int middle = size / 2;
        int index = 0;
        Node current = first;

        while (current != null) {

            if (index == middle) {
                System.out.println(current.value);
                break;
            }
            current = current.next;
            index++;
        }
    }


    //Task 4
    //Time Complexity - O(n)
    public int getKthFromEnd(int k) {

        Node current = first;
        int position = 0;

        while (current != null) {
            if (position == size - 1 - k) {
                return current.value;
            }
            current = current.next;
            position++;
        }

        throw new IllegalArgumentException();

    }


    //Task 5
    //Time Complexity - O(n)
    public void reverse(){
        Node previous = null;
        Node next;
        Node current = first;

        while (current != null)
        {
            next = current.next;
            current.next = previous;
            previous = current;
            current = next;
        }
        first = previous;
    }


    //Task 5
    //Time Complexity - O(n)
    public void reverse2(){
        Node current = first;
        Node per = null;
        Node next;
        while (current != null) {
            next = first.next;
            first = last;
            last.next = next;
        }
        first = per;
    }


}
