package org.example.lesson4.stack;



public class Main {
    public static void main(String[] args) {
        Stack stack = new Stack(5);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
//
        System.out.println(stack);
//
        stack.pop();
        stack.pop();
        System.out.println(stack);
        System.out.println("min " + stack.min());
//
        System.out.println("--------------------------------------");
//
//
        TwoStacks twoStacks = new TwoStacks(5);
        twoStacks.push1(1);
        twoStacks.push1(2);
        twoStacks.push2(3);
        twoStacks.push2(4);
//
        twoStacks.print();

//        Solution solution = new Solution();
//        char[] s = {'h', 'e', 'l', 'l', 'o'};
//        solution.reverseString(s);

    }
}
