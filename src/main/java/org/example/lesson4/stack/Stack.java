package org.example.lesson4.stack;

import java.util.Arrays;

public class Stack {

    private int[] items;
    private int count;


    public Stack(int n) {
        items = new int[n];
    }

    public void push(int item) {
        if (count == items.length) {
            throw new StackOverflowError();
        }
        items[count++] = item;
    }

    public int peek(){
        if (count == 0) {
            throw new IllegalStateException();
        }
        return items[count - 1];
    }


    public int pop(){
        if (count == 0) {
            throw new IllegalStateException();
        }
        return items[--count];
    }


    public String toString() {
        int[] content = Arrays.copyOfRange(items, 0, count);
        return Arrays.toString(content);
    }

    //Task3
    //Time complexity - O(n)
    public int min(){
        if (count == 0) {
            throw new IllegalStateException();
        }
        int m = items[0];
        for (int i = 1; i < count; i++) {
            if (m > items[i]) {
                m = items[i];
            }
        }
        return m;
    }


    //Task4
    //Time complexity - O(n)
    public int max(){
        if (count == 0) {
            throw new IllegalStateException();
        }
        int m = items[0];
        for (int i = 1; i < count; i++) {
            if (m < items[i]) {
                m = items[i];
            }
        }
        return m;
    }
}
