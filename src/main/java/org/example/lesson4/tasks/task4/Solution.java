package org.example.lesson4.tasks.task4;

public class Solution {
    //Time complexity - O(n)
    public int max(int count, int items[]){
        if (count == 0) {
            throw new IllegalStateException();
        }
        int m = items[0];
        for (int i = 1; i < count; i++) {
            if (m < items[i]) {
                m = items[i];
            }
        }
        return m;
    }}
