package org.example.lesson4.tasks.task3;

public class Solution {
    //Time complexity - O(n)
    public int min(int count, int items[]){
        if (count == 0) {
            throw new IllegalStateException();
        }
        int m = items[0];
        for (int i = 1; i < count; i++) {
            if (m > items[i]) {
                m = items[i];
            }
        }
        return m;
    }}
