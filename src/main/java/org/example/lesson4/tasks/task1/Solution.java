package org.example.lesson4.tasks.task1;


import java.util.Stack;

public class Solution {
    public void reverseString(char[] s) {
        int len = s.length;
        Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < len; i++) {
            stack.push(s[i]);
        }

        for (int i = 0; i < len; i++) {
            s[i] = stack.peek();
            stack.pop();
        }
        System.out.println(s);
    }
}