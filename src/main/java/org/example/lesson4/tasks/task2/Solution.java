package org.example.lesson4.tasks.task2;

import java.util.Stack;

public class Solution {
    public boolean isValid(String s) {
        char[] arr = s.toCharArray();
        Stack<Character> left = new Stack<>();
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] =='(' || arr[i] == '{' || arr[i] == '[') left.push(arr[i]);
            else if(arr[i]==')' && !left.isEmpty() && left.peek() == '(') left.pop();
            else if(arr[i]=='}' && !left.isEmpty() && left.peek() == '{') left.pop();
            else if(arr[i]==']' && !left.isEmpty() && left.peek() == '[') left.pop();
            else return false;
        }
        return left.size() == 0;
    }
}
