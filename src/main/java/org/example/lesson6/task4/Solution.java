package org.example.lesson6.task4;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        System.out.print("Enter size array: ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();

        Map<Integer, Integer> map = new HashMap<>();
        Map<Integer, Integer> map2 = new HashMap<>();
        System.out.print("Enter integer array: ");
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
            map.put(arr[i], 0);
        }

        System.out.print("Enter number k: ");
        scanner = new Scanner(System.in);
        int k = scanner.nextInt();

        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            for (int num : arr) {
                if (num - entry.getKey() == k || entry.getKey() - num == k) {
                    map2.put(entry.getKey(), num);
                }
            }
        }
        int mapSize = map2.size();
        int i = 0;
        for (Map.Entry<Integer, Integer> entry : map2.entrySet()) {
            if (i < mapSize / 2) {
                System.out.print("{" + entry.getKey() + ", " + entry.getValue() + "} ");
            } else {
                break;
            }
            i++;

        }



    }
}
