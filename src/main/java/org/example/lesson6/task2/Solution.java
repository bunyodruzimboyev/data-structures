package org.example.lesson6.task2;

import java.util.HashMap;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        System.out.print("Enter text: ");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        text.trim();
        int size = text.length();
        String text2 = text.toLowerCase();
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < size; i++) {
            if (map.containsKey(text2.charAt(i))) {
                Integer integer = map.get(text2.charAt(i));
                map.remove(text2.charAt(i), integer);
                integer++;
                map.put(text2.charAt(i), integer);
            } else  {
                map.put(text2.charAt(i), 1);
            }
        }
        for (int i = 0; i < size; i++) {
            Integer integer = map.get(text2.charAt(i));
            if (integer == 1 && text2.charAt(i) != ' ') {
                System.out.println(text.charAt(i));
                break;
            }
        }
    }
}
