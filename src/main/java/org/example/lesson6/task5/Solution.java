package org.example.lesson6.task5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Solution {
    public static HashMap<Integer, List<Integer> > res = new HashMap<>();
    public static int [] arr = {2, 6, 11, 17};
    public static void main(String[] args) {
        int target = 8;
        res.put(0,new ArrayList<>());

        for (int i = 0; i < arr.length; i++) {

            HashMap<Integer, List<Integer> > temp = new HashMap<>();
            int finalI = i;
            res.forEach((integer, integers) -> {
                List<Integer> t = new ArrayList<>();
                t.addAll(integers);
                t.add(finalI);
                temp.put(integer+arr[finalI],t);
            });

            temp.forEach((integer, integers) ->
                    res.put(integer,integers)
                    );
        }
        res.forEach((integer, integers) -> {
          if (integer == target)  System.out.println(integer + " " + integers.toString());
        });
    }

}