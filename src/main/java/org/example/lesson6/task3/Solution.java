package org.example.lesson6.task3;

import java.util.*;

public class Solution {
    public static void main(String[] args) {

        System.out.print("Enter text: ");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        text.trim();
        int size = text.length();
        String text2 = text.toLowerCase();

        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < size; i++) {
            if (map.containsKey(text2.charAt(i))) {
                Integer integer = map.get(text2.charAt(i));
                map.remove(text2.charAt(i), integer);
                integer++;
                map.put(text2.charAt(i), integer);
            } else  {
                map.put(text2.charAt(i), 1);
            }

        }

        Collection<Integer> values = map.values();
        int m = 0;
        for (Integer value : values) {
            if (value > m) {
                m = value;
            }
        }

        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue().equals(m)) {
                System.out.println(entry.getKey());
                break;
            }
        }



    }
}
