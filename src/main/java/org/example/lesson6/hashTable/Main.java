package org.example.lesson6.hashTable;



public class Main {
    public static void main(String[] args) {

        HashTable map = new HashTable();

        map.put(1, "Data");
        map.put(2, "Structures");
        map.put(3, "And");
        map.put(4, "Algorithms");
        map.put(5, "Course");


        System.out.println(map);
        System.out.println(map.get(3));
        System.out.println();
        map.remove(3);
        System.out.println(map);
        System.out.println(map.size());
    }

    public static int hash(int number) {
        return number % 100;
    }
}
