package org.example.stream;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("Darsda o`tilgan #BinaryTree da hamma barglarini #chop qiladigan #imkoniyatni qo`shing. #public void printAllLeaves()");
        list.add("#Vazifalar bo'limida yozilgan #xabarlar ballingizga ta'sir qiladi.");
        list.add("Darsda o`tilgan #BinaryTree da gi eng kichik elementni topadigan imkoniyatni #chopic qo`shing. #public void min()");
        list.add("Darsda o`tilgan #binaryTree da gi eng kichik elementni topadigan imkoniyatni qo`shing. #public void min()");
        list.add("#Vazifalar bo'limida yozilgan #xabarlar ballingizga ta'sir qiladi.");
        list.add("Darsda o`tilgan #BinaryTree da gi eng katta elementni topadigan imkoniyatni qo`shing. #public void max()");
        list.add("Darsda o`tilgan #BinaryTree da gi eng katta elementni topadigan imkoniyatni qo`shing. #public void max()");

        long bt = System.currentTimeMillis();
        List<String> collect = list.stream()
                .flatMap(s -> Stream.of(s.split(" ")))
                .filter(s -> s.startsWith("#"))
                .map(s -> s.substring(1).toLowerCase())
                .collect(Collectors.groupingBy(String::toString, Collectors.counting())).entrySet()
                .stream()
                .sorted(Map.Entry.<String, Long>comparingByValue().reversed().thenComparing(Map.Entry.<String,Long>comparingByKey()))
                .collect(Collectors.toList())
                .stream()
                .map(entry -> entry.getKey())
                .collect(Collectors.toList());

        collect.forEach(System.out::println);
        System.out.println(System.currentTimeMillis() - bt);
        //Eski texnologiya
        long dt = System.currentTimeMillis();
        List<String> word = new ArrayList<>();
        for (String s : list) {
            word.addAll(Arrays.asList(s.split(" ")));
        }
        List<String> res = new ArrayList<>();
        for (String s : word) {
            if(s.startsWith("#")) res.add(s.substring(1).toLowerCase());
        }
        HashMap<String,Long> ans = new HashMap<>();
        for (String s : res) {
            if(ans.containsKey(s))
            {
                long l = ans.get(s).longValue();
                l++;
                ans.put(s,l);
            }
            else ans.put(s,1L);
        }
        List<Map.Entry<String, Integer>> entries = new ArrayList(ans.entrySet());
        Collections.sort(entries, (o1, o2) -> o1.getKey().compareTo(o2.getKey()));
        entries.forEach(stringIntegerEntry -> System.out.println(stringIntegerEntry.getKey()));
        //Collections.sort(ans.entrySet());
        System.out.println(System.currentTimeMillis() - dt);
//        List<Person> answer = new ArrayList<>();
//        for (Person person : all) {
//            if (person.getFirstname().startsWith("A")) answer.add(person);
//        }
//
//        answer.forEach(System.out::println);

//        List<Person> res = all.stream()
//                .filter(person -> person.getFirstname().startsWith("A") && person.getPhone().startsWith("+99893") || person.getGender()==GenderEnum.MALE)
//                .collect(Collectors.toList());
//
//        res.forEach(System.out::println);

        //Filter
        //Map

//        List<PersonDTO> answer = new ArrayList<>();
//        for (Person person : all) {
//            PersonDTO pd = new PersonDTO(person.firstname, person.phone);
//           answer.add(pd);
//        }
//
//        answer.forEach(System.out::println);
//
//        List<PersonDTO> collect = all.stream()
//                .map(person -> {
//                    System.out.println("operatsiya");
//                    return new PersonDTO(person.firstname, person.phone);
//                })
//                .collect(Collectors.toList());
//
//        collect.forEach(System.out::println);
//
//        List<PersonDTO> a = all.stream()
//                .filter(person -> person.getFirstname().startsWith("A"))
//                .map(person -> new PersonDTO(person.firstname, person.phone))
//                .collect(Collectors.toList());
//
//        a.forEach(System.out::println);
//

        //Sort

//        List<Person> collect = all.stream()
//                .sorted(Comparator.comparing(Person::getAge).thenComparing(Person::getFirstname).thenComparing(Person::getLastname))
//                .collect(Collectors.toList());
//        collect.forEach(System.out::println);


        //All match

//
//        boolean b = all.stream()
//                .allMatch(person -> person.getAge() > 12);
        // System.out.println(b);

        //Any match
//        boolean b = all.stream()
//                .anyMatch(person -> person.getAge() > 12);
//
//        System.out.println(b);
        //None match

//        boolean b = all.stream()
//                .noneMatch(person -> person.getAge() < 10);
//        System.out.println(b);

        //Max

//        Person person = all.stream()
//                .max(Comparator.comparing(Person::getAge))
//                .get();
//
//
//
//        System.out.println(person);


        //Min


//        Person person = all.stream()
//                .min(Comparator.comparing(Person::getAge))
//                .get();
//
//
//        System.out.println(person);

//        Map<Integer, List<Person>> collect =
//                all
//                        .stream()
//                        .collect(Collectors.groupingBy(Person::getAge));
//
//        collect.forEach((integer, people) ->
//                System.out.println(integer + " " + people)
//        );

    }

    public static List<Person> getAll() {
        List<Person> res = new ArrayList<>();
        res.add(new Person("Ali", "Valiyev", "+998932652252", GenderEnum.MALE, 20));
        res.add(new Person("Aziz", "Ahmdeov", "+998932652252", GenderEnum.MALE, 21));
        res.add(new Person("Jamshid", "Turdiyev", "+998952652252", GenderEnum.MALE, 18));
        res.add(new Person("Niyoz", "Suyunov", "+998992652252", GenderEnum.MALE, 33));
        res.add(new Person("Jamshid", "Begliyev", "+998972652252", GenderEnum.MALE, 22));
        res.add(new Person("Xayri", "Toshmatov", "+998992652252", GenderEnum.MALE, 25));
        res.add(new Person("Jonibek", "Safarov", "+998902652252", GenderEnum.MALE, 27));
        res.add(new Person("Alisher", "Maqsudov", "+998902652252", GenderEnum.MALE, 11));
        res.add(new Person("Jahon", "Nurillayev", "+998912652252", GenderEnum.MALE, 21));
        res.add(new Person("Ahmed", "Valiyev", "+998932652252", GenderEnum.MALE, 18));
        res.add(new Person("Aziza", "Negmatova", "+998932652252", GenderEnum.FEMALE, 17));
        res.add(new Person("Kamola", "Soipova", "+998942652252", GenderEnum.FEMALE, 12));
        res.add(new Person("Zebo", "Negmatova", "+998972652252", GenderEnum.FEMALE, 22));
        res.add(new Person("Yulduz", "Turdiyeva", "+998972652252", GenderEnum.FEMALE, 13));
        res.add(new Person("Zebo", "Turdiyeva", "+998972652252", GenderEnum.FEMALE, 18));
        return res;
    }
}